# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  ZeroClipboard.config
    swfPath: '//cdnjs.cloudflare.com/ajax/libs/zeroclipboard/2.1.6/ZeroClipboard.swf'

  $link = $ '.short-link'
  $copy = $ '.copy-link-btn'

  client = new ZeroClipboard $copy
  client.on 'ready', () ->
    client.on 'aftercopy', (evt) ->
      alert 'Copied: ' + evt.data['text/plain']

  $ '.username'
    .on 'keyup', () ->

      if @.value
        shortLink = location + @.value

        $link.html shortLink
        $link.attr 'href', shortLink

        $copy.attr 'data-clipboard-text', shortLink
        $copy.removeAttr 'disabled'
      else
        $copy.attr 'disabled', 'disabled'
