class HomeController < ApplicationController
  def index
    @subscribe = Subscribe.new
    @populars = Redirect.popular.all
  end

  def subscribe
    @subscribe = Subscribe.new(subscribe_params)
    respond_to do |format|
      if @subscribe.save
        @subscribe = Subscribe.new
        format.js
      else
        format.js { render 'subscribe_error' }
      end
    end
  end

  private
    def subscribe_params
      params.require(:subscribe).permit(:email)
    end
end
