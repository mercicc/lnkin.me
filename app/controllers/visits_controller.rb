require "net/http"

class VisitsController < ApplicationController
  def visit
    @url = 'https://www.linkedin.com/in/' + params[:name]
    if profile_exist? @url
      ahoy.track 'redirect', name: "#{params[:name]}"
      Redirect.count_by_name(params[:name])
    end
    render layout: 'layouts/redirect'
  end

  protected

    def profile_exist?(url)
      client = LinkedIn::Client.new('75zusmi7s2x4xn', 'A5X5ZcrWZOk4MYpv')
      client.authorize_from_access("d9610423-f99c-435d-99de-ee2c6faf976d", "b71e557d-15e5-4dd8-bef6-7d5430e3d018")
      begin
        client.profile(url: url)
        true
      rescue
        false
      end
    end
end
