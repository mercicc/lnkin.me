class Redirect < ActiveRecord::Base
  scope :popular, -> { limit(10).order('counts desc') }

  def self.count_by_name(name)
    redirect = Redirect.find_or_create_by(name: name)
    redirect.counts += 1
    redirect.save
  end
end
