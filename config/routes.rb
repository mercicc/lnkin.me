Rails.application.routes.draw do
  root :to => 'home#index'
  get ':name' => 'visits#visit'
  post 'subscribe' => 'home#subscribe', as: 'subscribe'
end
