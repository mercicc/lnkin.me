class CreateRedirects < ActiveRecord::Migration
  def change
    create_table :redirects do |t|
      t.string :name
      t.integer :counts, default: 0

      t.timestamps
    end
  end
end
