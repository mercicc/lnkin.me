class CreateSubscribes < ActiveRecord::Migration
  def change
    create_table :subscribes do |t|
      t.string :email, null: false, default: ""

      t.timestamps
    end
    add_index :subscribes, :email, unique: true
  end
end
